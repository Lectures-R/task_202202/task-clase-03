---
pagetitle: "Task clase-03"
output:
  html_document:
    self_contained: FALSE
---

# Task-03

Escriba sus respuestas sobre el script y luego haga clic sobre la pestaña *run* (esquina inferior derecha). Esto le permitirá validar las respuestas, si lo respondió correctamente le retornará un mensaje de **Bien hecho!**. Puede validar las librerías activas en la sesión corriendo la función `sessionInfo()` sobre la consola.


```{r,include=FALSE}
rm(list=ls())
library(pacman)
p_load(tutorial,tidyverse,testthat,testwhat)
tutorial::go_interactive(height = 800)
```

<!--html_preserve-->
```{r ex="create_a", type="pre-exercise-code"}
library(dplyr)
```

```{r ex="create_a", type="sample-code"}
# Punto 1

# Cree un objeto de tipo carácter que contenga las primeras 10 letras del alfabeto
# Guárdelo como vector_1.

# Punto 2 
# Cree un vector con los números de 1 al 100 (puede usar funciones o con secuencias). 
# Guárdelo como vector_2.

# Punto 3
# Extraiga los 40 últimos elementos del vector.
# Guárdelo como vector_3.

# Punto 4
# Elabore una lista que con los objetos que ha creado. 
# Guárdelo como lista_1.

# Punto 5
# Cree un dataframe con los elementos vector_1 y vector_2.
# Guárdelo como dataframe_1.
```

```{r ex="create_a", type="solution"}
# Punto 1

# Cree un objeto de tipo caracter que contenga las primeras 10 letras del alfabeto
# Guárdelo como vector_1.

vector_1 = c("a","b","c","d","e","f","g","h","i","j")

# Punto 2 
# Cree un vector con los números de 1 al 10 (puede usar funciones o con secuencias). 
# Guárdelo como vector_2.

vector_2 = seq(1:10)

# Punto 3
# Extraiga los 4 últimos elementos del vector.
# Guárdelo como vector_3.

vector_3 = vector_2[6:10]

# Punto 4
# Elabore una lista que con los objetos que ha creado. 
# Guárdelo como lista_1.

lista_1 = list(vector_1,vector_2,vector_3)

# Punto 5
# Cree un dataframe con los elementos vector_1 y vector_2.
# Guárdelo como dataframe_1.

dataframe_1 = data.frame(vector_1,vector_2)
```

```{r ex="create_a", type="sct"}
ex() %>% check_object("dataframe_1") %>% check_equal()
success_msg("¡Bien hecho!")
```
<!--/html_preserve-->
